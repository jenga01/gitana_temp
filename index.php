<?php
/**
 * Created by PhpStorm.
 * User: jbogd
 * Date: 3/29/2019
 * Time: 4:13 PM
 */
include_once 'fc_cf.php';

?>


<html>


<head>
    <title>
        temperatūros konverteris
    </title>

    <style>

        body{
            margin: 8px !important;
        }

        .btn.btn-primary.btn-md{

            margin-top: 8px !important;
        }

    </style>

    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>

<body>

<form method="post">
<input name="fah" type="number" placeholder="farenheito vertė" class="form-control input-sm" required>
<input type="submit" name="sumbit_f" value="paskaičiuoti" class="btn btn-primary btn-md">

</form>


<form method="post">
    <input name="cel" type="number" placeholder="celsijaus vertė" class="form-control input-sm" required>
    <input type="submit" name="sumbit_c" value="paskaičiuoti" class="btn btn-primary btn-md">

</form>


<div>

    <?php

    fahrenheitToC();
    celsiusToF();

    ?>

</div>


</body>


</html>

